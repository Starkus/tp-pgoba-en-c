#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

typedef uint8_t byte;
typedef uint16_t index_t;

#define FULLBYTE (byte)(~((byte) 0))

void clearCollection(byte* start, size_t size) {
	for (int i=0; i < size; ++i) {
		start[i] = 0;
	}
}

int isFull(byte* start, size_t size) {
	for (int i=0; i < size; ++i) {
		if (start[i] != FULLBYTE)
			return 0;
	}
	return 1;
}

void randomizePack(index_t* pack, size_t size, size_t colSize) {
	for (int i=0; i < size; ++i) {
		pack[i] = (index_t) (rand() % (colSize * 8));
	}
}

void apply(index_t* pack, size_t size, byte* collection) {
	for (int i=0; i < size; ++i) {
		const index_t index = pack[i];
		const size_t whichByte = index / (sizeof(byte) * 8);
		const uint8_t whichBit = index % (sizeof(byte) * 8);

		collection[whichByte] |= 1 << whichBit;
	}
}

void printCollection(byte* start, size_t size) {
	for (int i=0; i < size; ++i) {
		for (int j=0; j < 8; ++j) {
			printf(start[i] & (1 << j) ? "1" : "0");
		}
		printf(" ");
	}
	printf("\n");
}

void printCollectionHex(byte* start, size_t size) {
	for (int i=0; i < size; ++i) {
		printf("%X ", start[i]);
	}
	printf("\n");
}

int main(int argc, char** argv) {
	// Random seed
	srand(time(NULL));

	const size_t colSize = 670 / 8;
	byte* collection = malloc(colSize * sizeof(byte));
	clearCollection(collection, colSize);

	// Allocate a pack buffer
	const size_t packSize = 5;
	index_t* pack = malloc(packSize * sizeof(index_t));

	float average = 0;

	for (int i=0; i < 4096; ++i) {
		clearCollection(collection, colSize);
		int packCounter = 0;
		while (!isFull(collection, colSize)) {
			randomizePack(pack, packSize, colSize);
			apply(pack, packSize, collection);
			++packCounter;

			//printCollectionHex(collection, colSize);
		}

		average += packCounter;
		//printf("Collection of %d completed after %d packs of %d each\n", colSize, packCounter, packSize);
	}

	average /= 4096;
	printf("\nAverage packs: %f\n", average);

	free(collection);
	free(pack);
}
